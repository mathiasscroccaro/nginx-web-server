#!/bin/sh

BASE_FOLDER=~/nginx

cd $BASE_FOLDER

container_id=$(docker ps -a -q --filter="name=nginx")
[ ! -z $container_id ] && docker stop $container_id && docker rm $container_id

image_id=$(docker images --format '{{.Repository}}:{{.Tag}}' | grep 'nginx')
[ ! -z $image_id ] && docker rmi $image_id

docker-compose build
docker-compose up -d

