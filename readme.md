# API Gateway for my personal server

My VPS is responsible for serving a personal web page and private projects. The entry point for all the requests is handled by a container running an Nginx instance, which works as an API gateway. The API gateway selects and redirects the data flow to a microservice depending on the type of the request. All the microservices are running in Docker containers - most connected through a unix socket, ensuring modularity, easy maintenance and security to the system.

![API Gateway diagram](api_gateway.png)

Checkout at [my website](http://www.mathias.dev.br)

<!--

## Example of directory structure

```
├── www
│   ├── app
│   │   ├── project1
│   │   │   ├── index.html
│   │   ├── project2
│   │   │   ├── index.html
└───── portfolio
        └── index.html
```

-->

